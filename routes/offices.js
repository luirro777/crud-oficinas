var express = require('express');
var router = express.Router();
const officesController = require('../controllers/offices');

router.get('/', officesController.listOffices ); // Lista todas las oficinas
router.get('/add', officesController.addOfficeGet); //Muestra formulario de agregado
router.post('/add', officesController.addOfficePost);// Procesa la solicitud de agregar una oficina
router.get('/edit/:id', officesController.editOfficeGet);// mostrar el formulario de edición de una oficina
router.post('/edit/:id', officesController.editOfficePost);// procesar la actualización de una oficina
router.get('/delete/:id', officesController.deleteOfficeGet);// mostrar la confirmación de eliminación de una oficina
router.post('/delete/:id', officesController.deleteOfficePost);// procesar la eliminación de una oficina
router.get('/search', officesController.searchOffice) //Muestra el formulario de busqueda
router.get('/search/results', officesController.searchOfficeResults);//Procesa la busqueda y muestra los resultados

module.exports = router;