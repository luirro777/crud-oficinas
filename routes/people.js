var express = require('express');
var router = express.Router();
const peopleController = require('../controllers/people');

router.get('/', peopleController.listPeople); // Lista de personas 
router.get('/add', peopleController.addPersonGet); //Muestra formulario de agregado 
router.post('/add', peopleController.addPersonPost); // Procesa solicitud de agregado
router.get('/edit/:id', peopleController.editPersonGet); // Muestra formulario de edicion 
router.post('/edit/:id', peopleController.editPersonPost); //Procesa solicitud de edicion
router.get('/delete/:id', peopleController.deletePersonGet) ;
router.post('/delete/:id', peopleController.deletePersonPost); 
router.get('/search', peopleController.searchPerson);// Muestra formulario de busqueda de una persona
router.get('/search/results', peopleController.searchPersonResults); //Procesa la busqueda
  
module.exports = router;