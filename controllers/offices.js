var express = require('express');

const listOffices = (req, res, next) => {
    const db = req.app.get("db");
    const query = 'SELECT * FROM offices'
    db.all(query, [], (err, rows) => {
      if (err) throw err;
      res.render('office-list', { offices: rows });
    });
  }

const addOfficeGet = (req, res, next) => {
    res.render('office-form', { action: 'Add', office: {} });
}

const addOfficePost = (req, res, next) => {
    const db = req.app.get("db");
    const { denomination } = req.body;
    const query = 'INSERT INTO offices (denomination) VALUES (?)'
    db.run( query, [denomination], (err) => {
      if (err) throw err;
      res.redirect('/offices');
    });
}

const editOfficeGet = (req, res, next) => {
  const id = req.params.id;
  const db = req.app.get("db");
  const query = 'SELECT * FROM offices WHERE id = ?';
  db.get(query, [id], (err, row) => {
    if (err) throw err;
    res.render('office-edit', { office: row });
  });
}

const editOfficePost = (req, res, next) => {
  const id = req.params.id;
  const db = req.app.get("db");
  const { denomination } = req.body;
  const query = 'UPDATE offices SET denomination = ? WHERE id = ?';
  db.run(query, [denomination, id], (err) => {
    if (err) throw err;
    res.redirect('/offices');
  });
}

const deleteOfficeGet = (req, res, next) => {
  const id = req.params.id;
  const db = req.app.get("db");
  const query = 'SELECT * FROM offices WHERE id = ?';
  db.get(query, [id], (err, row) => {
    if (err) throw err;
    res.render('confirm-delete', { office: row });
  });
}

const deleteOfficePost = (req, res, next) => {
  const id = req.params.id;
  const db = req.app.get("db");
  const query = 'DELETE FROM offices WHERE id = ?';
  db.run(query, [id], (err) => {
    if (err) throw err;
    res.redirect('/offices');
  });
}

const searchOffice = (req, res, next) => {
  res.render('office-search', {});
}

const searchOfficeResults = (req, res, next) => {
  const db = req.app.get("db");
  const keyword = req.query.keyword;
  const query = 'SELECT * FROM offices WHERE denomination LIKE ?';
  db.all(query, [`%${keyword}%`], (err, rows) => {
    if (err) throw err;
    res.render('office-results', {offices:rows});
  });
}

module.exports = {
    listOffices,
    addOfficeGet,
    addOfficePost,
    editOfficeGet,
    editOfficePost,
    deleteOfficeGet,
    deleteOfficePost,
    searchOffice,
    searchOfficeResults
}