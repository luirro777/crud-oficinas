var express = require('express');

const listPeople = (req, res, next) => {
    const db = req.app.get("db");
    //const query = 'SELECT people.*, offices.denomination AS office_denomination FROM people LEFT JOIN offices ON people.office_id = offices.id';
    const query = "SELECT people.id, people.name, people.email, offices.denomination FROM people JOIN offices WHERE people.office_id = offices.id"
    db.all(query, [], (err, rows) => {
      if (err) throw err;
      res.render('person-list', { people: rows, title: "Lista de personas" });
    });
}

const addPersonGet = (req, res, next) => {
    const db = req.app.get("db");
    db.all('SELECT * FROM offices', [], (err, rows) => {
      if (err) throw err;
      res.render('person-form', { action: 'Add', person: {}, offices: rows });
    });
}

const addPersonPost = (req, res, next) => {
    const db = req.app.get("db");
    const { name, email, office_id } = req.body;
    db.run('INSERT INTO people (name, email, office_id) VALUES (?, ?, ?)', [name, email, office_id], (err) => {
      if (err) throw err;
      res.redirect('/people');
    });
}

const editPersonGet = (req, res, next) => {
  const db = req.app.get("db");
  const id = req.params.id;
  const queryPerson = 'SELECT * FROM people WHERE id = ?';
  const queryOffices = 'SELECT * FROM offices';
  db.get(queryPerson, [id], (err, row) => {
    if (err) throw err;
    db.all(queryOffices, [], (err, offices) => {
      if (err) throw err;
      res.render('person-edit', { person: row, offices: offices });
    });
  });
}

const editPersonPost = (req, res, next) => {
  const db = req.app.get("db");
  const id = req.params.id;
  const { name, email, office_id } = req.body;
  const query = 'UPDATE people SET name = ?, email = ?, office_id = ? WHERE id = ?';
  db.run(query, [name, email, office_id, id], (err) => {
    if (err) throw err;
    res.redirect('/people');
  });
}

const deletePersonGet = (req, res, next) => {
  const id = req.params.id;
  const db = req.app.get("db");
  const query = 'SELECT * FROM people WHERE id = ?';
  db.get(query, [id], (err, row) => {
    if (err) throw err;
    res.render('confirm-delete-person', { person: row });
  });
}

const deletePersonPost = (req, res, next) => {
  const id = req.params.id;
  const db = req.app.get("db");
  const query = 'DELETE FROM people WHERE id = ?';
  db.run(query, [id], (err) => {
    if (err) throw err;
    res.redirect('/people');
  });
}

const searchPerson = (req, res, next) => {
  res.render('person-search', {});
}

const searchPersonResults = (req, res, next) => {
  const db = req.app.get("db");
  const keyword = req.query.keyword;
  const query = 'SELECT * FROM people WHERE denomination LIKE ?';
  db.all(query, [`%${keyword}%`], (err, rows) => {
    if (err) throw err;
    res.render('person-results', {people:rows});
  });
}

module.exports = {
    listPeople,
    addPersonGet,
    addPersonPost,
    editPersonGet,
    editPersonPost,
    deletePersonGet,
    deletePersonPost,
    searchPerson,
    searchPersonResults
  };