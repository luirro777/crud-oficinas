# Aplicacion CRUD en node + express + sqlite

Sencilla aplicación CRUD desarrollada en NodeJS, utilizando Express para establecer rutas y vistas, y sqlite como
gestor de bases de datos.

Se compone básicamente de 2 tablas:

- Oficinas
- Personas

Se incluye la posibilidad de listar, agregar, editar e incluso borrar registros

# Instalación

`git clone https://gitlab.com/luirro777/crud-oficinas`

`cd crud-oficinas`

`npm install`

`npm start`


